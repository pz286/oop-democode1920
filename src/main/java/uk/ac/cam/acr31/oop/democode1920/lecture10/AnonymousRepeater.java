package uk.ac.cam.acr31.oop.democode1920.lecture10;

abstract class AnonymousRepeater {

  abstract void doWork(int i);

  void run(int count) {
    for (int i = 0; i < count; i++) {
      doWork(i);
    }
  }

  public static void main(String[] args) {

    int y = 3;
    AnonymousRepeater r =
        new AnonymousRepeater() {
          @Override
          void doWork(int i) {
            System.out.println(i + y);
          }
        };
    r.run(10);

    AnonymousRepeater r2 =
        new AnonymousRepeater() {
          @Override
          void doWork(int i) {
            System.out.println("Not counting");
          }
        };
    r2.run(10);
  }
}
