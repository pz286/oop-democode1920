package uk.ac.cam.acr31.oop.democode1920.lecture10;

interface Repeatable {
  void doWork(int i);
}

public class LambdaRepeater {

  private final Repeatable repeatable;

  public LambdaRepeater(Repeatable repeatable) {
    this.repeatable = repeatable;
  }

  void run(int c) {
    for (int i = 0; i < c; i++) {
      repeatable.doWork(i);
    }
  }

  public static void main(String[] args) {
    LambdaRepeater r = new LambdaRepeater(System.out::println);
    r.run(10);
  }
}
