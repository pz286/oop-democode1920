package uk.ac.cam.acr31.oop.democode1920.lecture11;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Readers {

  public static void main(String[] args) throws IOException {
    File book = downloadBook();

    // Demonstrate decorator and adaptor pattern
    try (BufferedReader is =
        new BufferedReader(  // decorator
            new InputStreamReader( // adapter
                new GZIPInputStream( // decorator
                        new FileInputStream(book) // concrete implementation
                ), StandardCharsets.UTF_8))) {
      String line;
      while ((line = is.readLine()) != null) {
        System.out.println(line);
      }
    }
  }

  // Magic for downloading a book from the Internet and saving it to disk.
  // There's more on this in Further Java next year
  private static File downloadBook() throws IOException {
    File destination = File.createTempFile("oop", "book.txt.gz");
    URL url = new URL("http://www.gutenberg.org/cache/epub/345/pg345.txt");
    try (InputStream is = url.openStream();
        OutputStream os = new GZIPOutputStream(new FileOutputStream(destination))) {
      byte[] buffer = new byte[1024];
      int read;
      while ((read = is.read(buffer)) != -1) {
        os.write(buffer, 0, read);
      }
    }
    return destination;
  }
}
