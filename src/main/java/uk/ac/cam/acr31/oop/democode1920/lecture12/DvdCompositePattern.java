package uk.ac.cam.acr31.oop.democode1920.lecture12;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

interface Watchable {
  int priceInPence();

  String title();
}

class Dvd implements Watchable {
  private final String title;
  private final int priceInPence;

  Dvd(String title, int priceInPence) {
    this.title = title;
    this.priceInPence = priceInPence;
  }

  @Override
  public int priceInPence() {
    return priceInPence;
  }

  @Override
  public String title() {
    return title;
  }

  @Override
  public String toString() {
    return String.format("%d: %s%n", priceInPence(), title());
  }
}

class BoxSet implements Watchable {

  private final List<Watchable> items;

  BoxSet(List<Watchable> items) {
    this.items = List.copyOf(items);
  }

  BoxSet(Watchable... items) {
    this.items = List.copyOf(Arrays.asList(items));
  }

  @Override
  public int priceInPence() {
    return (int) (items.stream().mapToInt(Watchable::priceInPence).sum() * 0.9);
  }

  @Override
  public String title() {
    return items.stream().map(Watchable::title).collect(Collectors.joining(", "));
  }

  @Override
  public String toString() {
    return String.format("%d: %s%n", priceInPence(), title());
  }
}

public class DvdCompositePattern {
  public static void main(String[] args) {
    Dvd m1 = new Dvd("Episode IV: A New Hope", 100);
    Dvd m2 = new Dvd("Episode V: The Empire Strikes Back", 100);
    Dvd m3 = new Dvd("Episode VI: Return of the Jedi", 100);
    BoxSet b1 = new BoxSet(m1, m2, m3);
    Dvd m4 = new Dvd("Episode I: The Phantom Menace", 5);
    Dvd m5 = new Dvd("Episode II: Attack of the Clones", 5);
    Dvd m6 = new Dvd("Episode III: Revenge of the Sith", 5);
    BoxSet b2 = new BoxSet(m4, m5, m6);
    BoxSet all = new BoxSet(b1, b2);
  }
}
