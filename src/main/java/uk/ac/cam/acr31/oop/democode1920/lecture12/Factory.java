package uk.ac.cam.acr31.oop.democode1920.lecture12;

import java.util.HashMap;
import java.util.Map;

public class Factory {

  private Map<String, Integer> used = new HashMap<>();
  private int counter = 0;

  static class SpecialThing {

    private final String name;
    private final int id;

    private SpecialThing(String name, int id) {
      this.name = name;
      this.id = id;
    }
  }

  class AnotherOne {

    AnotherOne() {
      counter++;
    }
  }

  public SpecialThing newSpecialThing(String name) {
    if (!used.containsKey(name)) {
      used.put(name, counter++);
    }
    return new SpecialThing(name, used.get(name));
  }

  public static void main(String[] args) {

    Factory factory1 = new Factory();
    Factory factory2 = new Factory();

    SpecialThing one = factory1.newSpecialThing("apple");
    SpecialThing two = factory2.newSpecialThing("orange");
  }
}
