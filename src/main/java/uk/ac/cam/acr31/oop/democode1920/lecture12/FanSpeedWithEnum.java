package uk.ac.cam.acr31.oop.democode1920.lecture12;

/**
 * Model a desk fan with three settings: off, slow and fast.
 *
 * <p>The fan has one button to click through these three settings.
 *
 * <p>The two methods on this class are called by the firmware of the fan. {@code click} is called
 * whenever someone presses the button to change setting. {@code update} is called whenever the
 * motor is ready to change speed.
 */
public class FanSpeedWithEnum {

  enum Speed {
    OFF,
    SLOW,
    FAST
  }

  private Speed state;

  /** Set the motor turning at the correct speed. */
  void update(MotorController motorController) {
    switch (state) {
      case OFF:
        motorController.stop();
        break;
      case SLOW:
        motorController.turnSlow();
        break;
      case FAST:
        motorController.turnFast();
        break;
    }
  }

  /** Respond to a button click to change between settings. */
  void click() {
    switch (state){
      case OFF:
        state = Speed.SLOW;
        break;
      case SLOW:
        state = Speed.FAST;
        break;
      case FAST:
        state = Speed.OFF;
        break;
    }
  }
}
