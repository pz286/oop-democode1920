package uk.ac.cam.acr31.oop.democode1920.lecture2;

public class WideningAndNarrowing {

  public static void main(String[] args) {
    byte b = 10;

    int i = b; // implicit widening

    //    byte c = i; // attempted implicit narrowing

    byte d = (byte) i; // explicit narrowing

    int j = 255;

    System.out.println(Integer.toBinaryString(j));

    byte z = (byte) j;

    System.out.println(z);
  }
}
