package uk.ac.cam.acr31.oop.democode1920.lecture3;

public class PrivateDemo {

  private int x;

  void add(PrivateDemo p) {
    p.x = 6;
  }
}
