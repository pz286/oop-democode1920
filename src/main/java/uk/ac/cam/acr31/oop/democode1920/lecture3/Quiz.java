package uk.ac.cam.acr31.oop.democode1920.lecture3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Quiz {

  void doQuiz() {

    List<String> choices = new ArrayList<>(List.of("one", "two", "three"));
    MultipleChoiceQuestion q = new MultipleChoiceQuestion("p", choices, 1);
    choices.set(1, "hahaha!");

    List<Question> questions =
        List.of(
            new Question("Should I indent source code with tabs or spaces?", "tabs"),
            new Question("Which is the best college?", "mine"));

    Scanner scanner = new Scanner(System.in);
    int correct = 0;
    for (Question question : questions) {
      question.ask();
      String response = scanner.nextLine();
      if (question.check(response)) {
        correct++;
      }
    }
    System.out.printf("You got %d correct %n", correct);
  }

  public static void main(String[] args) {
    Quiz quiz = new Quiz();
    quiz.doQuiz();
  }
}
