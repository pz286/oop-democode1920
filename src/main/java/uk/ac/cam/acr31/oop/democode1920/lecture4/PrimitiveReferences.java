package uk.ac.cam.acr31.oop.democode1920.lecture4;

public class PrimitiveReferences {

  private static int a(int x) {
    return x + 4;
  }

  private static void b(int x) {
    x += 4;
  }

  public static void main(String[] args) {
    int x = 1;
    int ax = a(x);
    b(x);
    System.out.printf("%d, %d%n", ax, x);
  }
}
