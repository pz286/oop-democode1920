package uk.ac.cam.acr31.oop.democode1920.lecture5;

public class Expression {

  private static final int LITERAL = 1;
  private static final int PLUS = 2;
  private static final int MULT = 3;

  private final int type;
  private final int value;
  private final Expression left;
  private final Expression right;

  public Expression(int type, int value, Expression left, Expression right) {
    this.type = type;
    this.value = value;
    this.left = left;
    this.right = right;
  }

  // factory method
  // static constructor
  static Expression literal(int v) {
    return new Expression(LITERAL, v, null, null);
  }

  static Expression plus(Expression left, Expression right) {
    return new Expression(PLUS, 0, left, right);
  }

  static Expression mult(Expression left, Expression right) {
    return new Expression(MULT, 0, left, right);
  }

  int evaluate() {
    switch (type) {
      case LITERAL:
        return value;
      case PLUS:
        return left.evaluate() + right.evaluate();
      case MULT:
        return left.evaluate() * right.evaluate();
    }
    throw new UnsupportedOperationException();
  }

  public String toString() {
    switch (type) {
      case LITERAL:
        return String.valueOf(value); //  return ""+value
      case PLUS:
        return String.format("(%s + %s)",left,right);
      case MULT:
        return String.format("(%s * %s)",left,right);
    }
    throw new UnsupportedOperationException();
  }
}
