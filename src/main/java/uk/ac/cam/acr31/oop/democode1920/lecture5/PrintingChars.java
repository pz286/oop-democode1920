package uk.ac.cam.acr31.oop.democode1920.lecture5;

public class PrintingChars {

  public static void main(String[] args) {
    String letters = "ABC";
    char[] numbers = {'1', '2', '3'};
    System.out.println(numbers);
    System.out.println(letters + " easy as " + numbers);
  }
}
