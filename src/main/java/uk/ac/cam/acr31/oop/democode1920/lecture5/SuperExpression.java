package uk.ac.cam.acr31.oop.democode1920.lecture5;

public abstract class SuperExpression {

  abstract int evaluate();
}
