package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing1;

public class Stick {

  private final int x;

  public Stick(int x) {
    this.x = x;
  }

  void draw(AsciiImage asciiImage) {
    asciiImage.draw(x, '|');
  }
}
