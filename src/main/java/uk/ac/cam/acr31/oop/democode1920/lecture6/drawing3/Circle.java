package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing3;

public class Circle extends Shape {

  private final int x;

  public Circle(int x) {
    this.x = x;
  }

  @Override
  void draw(AsciiImage asciiImage) {
    asciiImage.draw(x, 'o');
  }
}
