package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing3;

import java.util.List;

public class Drawing {

  public static void draw(List<Shape> shapes) {
    AsciiImage asciiImage = new AsciiImage();
    shapes.forEach(s -> s.draw(asciiImage));
    System.out.println(asciiImage);
  }

  public static void main(String[] args) {
    draw(List.of(new Circle(0), new Circle(5), new Square(1), new Square(4)));
  }
}
