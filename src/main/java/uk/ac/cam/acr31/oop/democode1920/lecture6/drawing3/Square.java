package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing3;

public class Square extends Shape {

  private final int x;

  public Square(int x) {
    this.x = x;
  }

  void draw(AsciiImage asciiImage) {
    asciiImage.draw(x, '#');
  }
}
