package uk.ac.cam.acr31.oop.democode1920.lecture6.fields;

public class Instrument extends Ceremony {
  boolean strings;
  boolean bow;

  boolean bow() {
    return bow;
  }

  static Instrument violin() {
    Instrument i = new Instrument();
    i.speech = "Shut up and listen";
    i.strings = true;
    i.bow = true;
    ((Ceremony) i).bow = false;
    return i;
  }
}
