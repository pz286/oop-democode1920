package uk.ac.cam.acr31.oop.democode1920.lecture7;

public class DynamicPolymorphism {

  static class A {
    String get = "A";

    String get() {
      return "A";
    }
  }

  static class B extends A {
    String get = "B";

    String get() {
      return "B";
    }
  }

  static void print(A value) {
    System.out.println(value.get);
    System.out.println(value.get());
  }

  public static void main(String[] args) {
    print(new B());
  }
}
