package uk.ac.cam.acr31.oop.democode1920.lecture8;

public class AutoBoxing {

  private static void takesPrimitive(int i) {
    int j = i+1;
    System.out.println(j);
  }

  private static void takesObject(Integer i) {
    int j = i+1;
    System.out.println(j);
  }

  public static void main(String[] args) {
    int i =1;
    takesPrimitive(i); // just pass int by value
    takesObject(i); // autobox int to Integer

    Integer j = new Integer(2);
    takesPrimitive(j); // autounbox Integer to int
    takesObject(j); // just pass reference to j by value

    Integer k = null;
    takesPrimitive(k); // attempt to unbox null to an int
    takesObject(k);

  }
}
