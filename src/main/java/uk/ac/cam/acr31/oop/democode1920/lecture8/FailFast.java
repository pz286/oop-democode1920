package uk.ac.cam.acr31.oop.democode1920.lecture8;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FailFast {

  public static void main(String[] args) {
    List<Integer> l = new ArrayList<>(List.of(1, 2, 3, 4, 5, 6));
    List<String> s = new ArrayList<>(List.of("a", "b", "c", "d", "e", "f"));

    for (int i = 0; i < l.size(); i++) {
      Integer v1 = l.get(i);
      String v2 = s.get(i);
      System.out.println(v1 + " " + v2);
    }

    for (Integer v1 : l) {
      System.out.println(v1);
    }

    Iterator<Integer> step1 = l.iterator();
    Iterator<String> step2 = s.iterator();
    while (step1.hasNext() && step2.hasNext()) {
      Integer v1 = step1.next();
      String v2 = step2.next();
      System.out.println(v1 + " " + v2);
    }

    int i = 0;
    for (Integer v : l) {
      if (i++ == 1) {
        l.remove(i);
      }
    }

    Iterator<Integer> step3 = l.iterator();
    int j = 0;
    while (step3.hasNext()) {
      Integer v = step3.next();
      if (j++ == 1) {
        step3.remove();
      }
    }
  }
}
