package uk.ac.cam.acr31.oop.democode1920.lecture9;

public class ComparableTest implements Comparable<ComparableTest> {

  private int x;
  private int y;

  /*  should implement a total order.
   *
   * 1) everything is comparable (connex property)
   * 2) a <= b and b <= c implies a <=c (transitivity)
   * 3) a <= b and b <= a implies a == b (antisymmetry)
   * and
   * 4) throw a NullPointerException if o is null
   */
  @Override
  public int compareTo(ComparableTest o) {
    int v = Integer.compare(x, o.x);
    if (v != 0) {
      return v;
    }
    return Integer.compare(y, o.y);
  }
}
