package uk.ac.cam.acr31.oop.democode1920.lecture9;

import java.io.FileOutputStream;
import java.io.IOException;

class MyException extends Exception {
  public MyException(String message) {
    super(message);
  }

  public MyException(String message, Throwable cause) {
    super(message, cause);
  }
}

public class ExceptionChaining {

  private static void doStuff() throws MyException {
    try (FileOutputStream fos = new FileOutputStream("/cantWrite.txt")) {
      // things
    } catch (IOException e) {
      // exception chaining
      throw new MyException(e.getMessage(), e);
    }
  }

  public static void main(String[] args) throws MyException {
    doStuff();
  }
}
